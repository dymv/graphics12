//
// Created by eugenedymov on 20.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <GLKit/GLKit.h>
#import "Fence.h"
#import "TextureManager.h"
#import "FenceDefines.h"
#import "Helpers.h"
#import "Defines.h"

@interface Fence ()
@property (nonatomic, retain) GLKTextureInfo *fenceTexture;

@end

@implementation Fence {
    GLfloat *_fenceNormals;
}

- (void)createObject {
    self.fenceTexture = [TextureManager loadTexture:@"fence-texture"];
    _fenceNormals = makeNormals(fence, ASIZE(fence), 0, 0, 1);
}

- (void)partExecute {
    glPushMatrix();
    glEnable(GL_TEXTURE_COORD_ARRAY);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GEQUAL, 0.4);

    glBindTexture(GL_TEXTURE_2D, self.fenceTexture.name);

    glVertexPointer(3, GL_FLOAT, 0, fence);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, fenceColors);
    glTexCoordPointer(2, GL_FLOAT, 0, fenceTextureM);
    glNormalPointer(GL_FLOAT, 0, _fenceNormals);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisable(GL_ALPHA_TEST);
    glDisable(GL_BLEND);
    glDisable(GL_TEXTURE_COORD_ARRAY);
    glDisable(GL_TEXTURE_2D);

    glPopMatrix();
}

- (void)execute {
    glPushMatrix();

    glPushMatrix();
    glTranslatef(1, -0.2, 6.);
    glScalef(5, 0.75, 1);
    [self partExecute];
    glPopMatrix();

    glPushMatrix();
    glTranslatef(1, -0.2, 11);
    glScalef(5, 0.75, 1);
    [self partExecute];
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0, -0.2, 12);
    glRotatef(90, 0, 1, 0);
    glScalef(5, 0.75, 1);
    [self partExecute];
    glPopMatrix();

    glPushMatrix();
    glTranslatef(5, -0.2, 12);
    glRotatef(90, 0, 1, 0);
    glScalef(5, 0.75, 1);
    [self partExecute];
    glPopMatrix();

    glPopMatrix();
}


@end