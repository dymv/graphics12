//
//  ViewController.h
//  Graphics
//
//  Created by Eugene Dymov on 28.10.12.
//  Copyright (c) 2012 Eugene Dymov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@interface ViewController : GLKViewController <UIGestureRecognizerDelegate>

@end
