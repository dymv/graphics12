//
// Created by eugenedymov on 15.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "Helpers.h"

@implementation Helpers {

}

/**
* 1  - make larger
* -1 - make smaller
* */
GLfloat makeNormal(GLfloat value, int type) {
    if (type > 0) {
        if (value > 0) {
            return value * 2.0 * type;
        } else if (fabsf(value)< 1e-5) {
            return 0.1 * type;
        } else if (value < 0) {
            return value / (2.0 * type);
        }
    } else if (type < 0) {
        if (value > 0) {
            return value / (2.0 * type);
        } else if (fabsf(value)< 1e-5) {
            return -0.1 * type;
        } else if (value < 0) {
            return value * 2.0  * type;
        }
    }
    return value;
}

GLvoid *makeNormals(GLfloat *array, unsigned long size, int byX, int byY, int byZ) {
    GLfloat *result = calloc(size, sizeof(GLfloat));
    for (int i = 0; i < size; i+=3) {
        result[i] = makeNormal(array[i], byX);
        result[i + 1] = makeNormal(array[i + 1], byY);
        result[i + 2] = makeNormal(array[i + 2], byZ);
    }

    return result;
}

GLvoid *multiply2DArray(GLfloat *array, unsigned long size, GLfloat x, GLfloat y) {
    GLfloat *result = calloc(size, sizeof(GLfloat));

    for (int i = 0; i < size; i+=2) {
        result[i] = array[i] * x;
        result[i + 1] = array[i + 1] * y;
    }

    return result;
}

@end