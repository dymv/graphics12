//
// Created by eugenedymov on 15.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "Builder.h"
#import "Defines.h"
#import "S21House.h"
#import "Road.h"
#import "S32House.h"
#import "Tree.h"
#import "S23House.h"
#import "Fence.h"

#define ASIZE(x) (sizeof((x)) / sizeof((x)[0]))

@interface Builder ()
@property (nonatomic, retain) S21House *s21House;
@property (nonatomic, retain) S23House *s23House;
@property (nonatomic, retain) House *s30House;
@property (nonatomic, retain) House *s32House;
@property (nonatomic, retain) House *s32k1House;
@property (nonatomic, retain) House *s32k2House;
@property (nonatomic, retain) House *s34House;
@property (nonatomic, retain) House *valut;
@property (nonatomic, retain) Road *road;
@property (nonatomic, retain) Tree *tree;


@property (nonatomic, retain) NSMutableArray *objects3d;


@end

@implementation Builder {
}

+ (Builder *)sharedBuilder {
    static Builder *_sharedBuilder;
    static dispatch_once_t onceSharedBuilderCreation;

    dispatch_once(&onceSharedBuilderCreation, ^ {
        _sharedBuilder = [[Builder alloc] init];
    });

    return _sharedBuilder;
}

- (id)init {
    self = [super init];
    if (self) {
        [self initial];
    }

    return self;
}

- (void)initial {
    self.s21House = [[[S21House alloc] init] autorelease];
    self.s23House = [[[S23House alloc] initWithPosition:Vertex3DMake(2, -0.2, 12)] autorelease];
    self.s30House = [House houseWithMainTexture:@"s30-texture" mainStretchFactor:1 sideTexture:@"s30v2-texture" sideStretchFactor:1 position:Vertex3DMake(-8, -0.2, 10) rotationAngle:90 rotation:Vertex3DMake(0, 1, 0) generalScale:Vertex3DMake(3, 1, 3)];
    self.s32House = [House houseWithMainTexture:@"s32-texture" mainStretchFactor:5 sideTexture:@"s32v2-texture" sideStretchFactor:1 position:Vertex3DMake(-6, -0.2, 3) rotationAngle:90 rotation:Vertex3DMake(0, 1, 0) generalScale:Vertex3DMake(3, 3, 1)];
    self.s32k1House = [House houseWithMainTexture:@"s32k1-texture" mainStretchFactor:1 sideTexture:@"s32k1v2-texture" sideStretchFactor:1 position:Vertex3DMake(-10, -0.2, 5) rotationAngle:90 rotation:Vertex3DMake(0,1, 0) generalScale:Vertex3DMake(1, 2.2, 3)];
    self.s32k2House = [House houseWithMainTexture:@"s32k2-texture" mainStretchFactor:1 sideTexture:@"s32k2v2-texture" sideStretchFactor:1 position:Vertex3DMake(-6, -0.2, 3) rotationAngle:-90 rotation:Vertex3DMake(0,1, 0) generalScale:Vertex3DMake(1, 2.2, 1)];
    self.s34House = [House houseWithMainTexture:@"s32-texture" mainStretchFactor:2 sideTexture:@"s32v2-texture" sideStretchFactor:1 position:Vertex3DMake(-6, -0.2, -17) rotationAngle:90 rotation:Vertex3DMake(0, 1, 0) generalScale:Vertex3DMake(3, 3, 1)];
    self.valut = [House houseWithMainTexture:@"vault-texture" mainStretchFactor:1 sideTexture:@"vaultv2-texture" sideStretchFactor:1 position:Vertex3DMake(3.5, -0.2, 6) rotationAngle:-90 rotation:Vertex3DMake(0, 1, 0) generalScale:Vertex3DMake(0.5, 0.5, 0.5)];
    self.road = [[[Road alloc] init] autorelease];

    [self setObjects3d:[NSMutableArray arrayWithObjects:
            self.s21House,
            self.s23House,
            self.s30House,
            self.s32House,
            self.s32k1House,
            self.s32k2House,
            self.s34House,
            self.valut,
            self.road,
            [Tree treeWithTexture:@"tree01-texture" position:Vertex3DMake(-4.5, -0.2, 0) angle:0 rotation:Vertex3DMake(0, 0, 0) scale:Vertex3DMake(2, 2, 2)],
            [Tree treeWithTexture:@"tree01-texture" position:Vertex3DMake(-4, -0.2, -1) angle:0 rotation:Vertex3DMake(0, 0, 0) scale:Vertex3DMake(1, 3, 1)],
            [Tree treeWithTexture:@"tree01-texture" position:Vertex3DMake(-4.5, -0.2, -3) angle:0 rotation:Vertex3DMake(0, 0, 0) scale:Vertex3DMake(2, 1, 2)],
            [Tree treeWithTexture:@"tree01-texture" position:Vertex3DMake(-4.3, -0.2, -5) angle:0 rotation:Vertex3DMake(0, 0, 0) scale:Vertex3DMake(1.5, 2.5, 1.5)],
            [Tree treeWithTexture:@"tree01-texture" position:Vertex3DMake(-7.3, -0.2, 3) angle:0 rotation:Vertex3DMake(0, 0, 0) scale:Vertex3DMake(2, 2.5, 2)],
            [[[Fence alloc] init] autorelease],
            nil]];
}

- (void)execute {
    glPushMatrix();

    for (Object3D *object3D in self.objects3d) {
        [object3D execute];
    }

    glPopMatrix();
}

@end