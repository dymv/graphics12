const GLfloat road[] = {
        -0.5, -0.2, 50,
        0.5, -0.2, 50,
        -0.5, -0.2, -50,
        0.5, -0.2, -50,
};

const GLubyte roadColors[] = {
        122, 122, 122, 255,
        122, 122, 122, 255,
        122, 122, 122, 255,
        122, 122, 122, 255,
};

const GLfloat roadTexture[] = {
        0,0,
        1,0,
        0,40,
        1,40
};

const GLfloat asphalt[] = {
        -50, -0.3, 50,
        50, -0.3, 50,
        -50, -0.3, -50,
        50, -0.3, -50,
};

const GLfloat asphaltTexture[] = {
        0,0,
        40,0,
        0,40,
        40,40
};