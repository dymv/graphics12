//
//  ViewController.m
//  Graphics
//
//  Created by Eugene Dymov on 28.10.12.
//  Copyright (c) 2012 Eugene Dymov. All rights reserved.
//

#import <GLKit/GLKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import "ViewController.h"
#import "Defines.h"
#import "Builder.h"
#import "UIView+FrameAccessor.h"
#import "Macros.h"
#import "TextureManager.h"

extern GLvoid *makeNormals(GLfloat *array, unsigned long size, int byX, int byY, int byZ);

@interface ViewController ()

@property (retain, nonatomic) EAGLContext *context;
@property (retain, nonatomic) GLKBaseEffect *effect;
@property (nonatomic, retain) IBOutlet UIButton *joystick;
@property (nonatomic, retain) IBOutlet UIControl *joystickBackground;
@property (nonatomic, retain) IBOutlet UIImageView *railgun;
@property (nonatomic, retain) NSTimer *movingTimer;
@property (nonatomic, retain) GLKTextureInfo *railgunTexture;


@property (nonatomic, assign) BOOL isRailgunVisible;
@property (nonatomic, assign) BOOL isRailgunRayVisible;
@end

@implementation ViewController {
    CGPoint gl_oldRotAng, gl_rotAng, gl_endRotAng;
    GLfloat gl_scale, gl_oldScale;
    CGFloat zPos, zScale;
    CGFloat xPos, xScale;
    GLuint texture[1];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }

    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    [self setIsRailgunVisible:NO];
    [self setIsRailgunRayVisible:NO];
    [self.railgun setY:self.view.width + self.railgun.height];

    self.context = [[[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1] autorelease];

    if (!self.context) {
        NSLog(@"Failed to create ES context.");
        abort();
    }

    GLKView *view = (GLKView *) self.view;
    [view setContext:self.context];
    [view setDrawableDepthFormat:GLKViewDrawableDepthFormat24];

    [EAGLContext setCurrentContext:self.context];

    zPos = -9;
    gl_scale = -10.0;
    gl_oldScale = -10.0;

    [self initLighting];
    [self setClipping];

    [self createGestureRecognizer];

    self.railgunTexture = [TextureManager loadTexture:@"railgun-ray-texture"];
}

- (void)createGestureRecognizer {
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] init];
    [panGestureRecognizer addTarget:self action:@selector(panGestureRecognizerActuated:)];
    [self.view addGestureRecognizer:panGestureRecognizer];
    [panGestureRecognizer release];

    UIPanGestureRecognizer *buttonPanGest = [[UIPanGestureRecognizer alloc] init];
    [buttonPanGest addTarget:self action:@selector(buttonPanGestureRecognizerActuated:)];
    [self.joystick addGestureRecognizer:buttonPanGest];
    [buttonPanGest release];

    UISwipeGestureRecognizer *quakeGest = [[UISwipeGestureRecognizer alloc] init];
    [quakeGest addTarget:self action:@selector(quakeGestActuated)];
    [quakeGest setDirection:UISwipeGestureRecognizerDirectionUp];
    [quakeGest setNumberOfTouchesRequired:2];
    [quakeGest setDelegate:self];
    [self.view addGestureRecognizer:quakeGest];
    [quakeGest release];

    UISwipeGestureRecognizer *quakeGestH = [[UISwipeGestureRecognizer alloc] init];
    [quakeGestH addTarget:self action:@selector(quakeGestActuated)];
    [quakeGestH setDirection:UISwipeGestureRecognizerDirectionDown];
    [quakeGestH setNumberOfTouchesRequired:2];
    [quakeGestH setDelegate:self];
    [self.view addGestureRecognizer:quakeGestH];
    [quakeGestH release];

    UITapGestureRecognizer *railgunShotGest = [[UITapGestureRecognizer alloc] init];
    [railgunShotGest setNumberOfTapsRequired:1];
    [railgunShotGest setNumberOfTouchesRequired:1];
    [railgunShotGest addTarget:self action:@selector(railgunShotGestActuated)];
    [self.railgun addGestureRecognizer:railgunShotGest];
    [railgunShotGest release];
}

- (void)railgunShotGestActuated {
    [self setIsRailgunRayVisible:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1500 * NSEC_PER_MSEC), dispatch_get_main_queue(), ^{
        [self setIsRailgunRayVisible:NO];
    });
    [self playRailgunShot];
}

- (void)quakeGestActuated {
    void (^animation)() = ^{
        if (self.isRailgunVisible) {
            [self.railgun setY:self.view.width + self.railgun.height];
        } else {
            [self.railgun setY:self.view.width - self.railgun.height];
        }
    };

    [UIView animateWithDuration:0.2
                          delay:0.0
                        options:0
                     animations:^{
                         animation();
                     }
                     completion:^(BOOL finished) {
                         [self setIsRailgunVisible:!self.isRailgunVisible];
                     }];

}

- (void)playRailgunShot {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"railgf1a" ofType:@"wav"];
    SystemSoundID soundID;
    NSURL *filePath = [NSURL fileURLWithPath:path isDirectory:NO];
    AudioServicesCreateSystemSoundID((CFURLRef)filePath, &soundID);
    AudioServicesPlaySystemSound(soundID);
}

- (void)buttonPanGestureRecognizerActuated:(UIPanGestureRecognizer *)gest {
    switch (gest.state) {
        case UIGestureRecognizerStatePossible:
            break;
        case UIGestureRecognizerStateBegan: {
            zScale = 0.0;
            xScale = 0.0;
            [self startMovingTimer];

            break;
        }
        case UIGestureRecognizerStateChanged: {


            CGPoint trans = [gest translationInView:self.joystick];
            CGPoint newOrigin = CGPointMake(self.joystick.x + trans.x, self.joystick.y + trans.y);
            [gest setTranslation:CGPointMake(0, 0) inView:self.joystick];

            if ( (newOrigin.x + self.joystick.width > self.joystickBackground.width) ||
                    (newOrigin.y + self.joystick.height > self.joystickBackground.height) ||
                    (newOrigin.x <= 0) ||
                    (newOrigin.y <= 0)
                    ) {
            } else {
                [self.joystick setOrigin:newOrigin];

                [self move];
            }

            break;
    }
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed: {
            [self stopMovingTimer];
            [UIView animateWithDuration:0.2
                                  delay:0.0
                                options:UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 CGPoint centered = CENTER_IN_PARENT(self.joystickBackground, self.joystick.width, self.joystick.height);
                                 [self.joystick setOrigin:centered];
                             }
                             completion:^(BOOL finished) {

                             }];

            break;
        }
    }
}

- (void)move {
    CGPoint centered = CENTER_IN_PARENT(self.joystickBackground, self.joystick.width, self.joystick.height);

    xScale = (-1.0 * (self.joystick.x - centered.x) / centered.y) / 16.0;
    zScale = (-1.0 * (self.joystick.y - centered.y) / centered.y) / 16.0;
}

- (void)startMovingTimer {
    if (self.movingTimer) {
        [self.movingTimer invalidate];
    }

    self.movingTimer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(movingTimerTick) userInfo:nil repeats:YES];
}

- (void)movingTimerTick {
    [self setIsRailgunRayVisible:NO];

    CGFloat cosA = cosf(DEGREES_TO_RADIANS(gl_rotAng.x));
    CGFloat sinA = sinf(DEGREES_TO_RADIANS(gl_rotAng.x));

    xPos += xScale * cosA - zScale * sinA;
    zPos += zScale * cosA + xScale * sinA;

    if (fabsf(xPos) >= 0.4) {
        xPos = 0.4 * xPos / fabsf(xPos);
    }

    if (zPos <= -16) {
        zPos = -16;
    }

    if (zPos >= 0) {
        zPos = 0;
    }

//    NSLog(@"xp:%f xs%f / zp:%f zs%f / %f", xPos, xScale, zPos, zScale, gl_rotAng.x);
}

- (void)stopMovingTimer {
    if (self.movingTimer) {
        [self.movingTimer invalidate];
        self.movingTimer = nil;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_context release];
    [super dealloc];
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect {
    glClearColor(0.33f, 0.51f, 0.74f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_DEPTH_TEST);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);

//    glEnable(GL_CULL_FACE);
//    glCullFace(GL_BACK);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glRotatef(gl_rotAng.x, 0, 1, 0);
    glTranslatef(0, 0, zPos);
    glTranslatef(xPos, 0, 0);

    [[Builder sharedBuilder] execute];

    [self drawRailgunRay];

    glDisableClientState(GL_COLOR_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

- (void)drawRailgunRay {
    if (!self.isRailgunRayVisible) {
        return;
    }

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

//    glRotatef(gl_rotAng.x, 0, 1, 0);
//    glTranslatef(0, 0, -zPos);
//    glTranslatef(-xPos, 0, 0);

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GEQUAL, 0.1);

    GLfloat railgunRay[] = {
            0.0,0.0,1.0,
            1.0,0.0,1.0,
            0.0,1.0,1.0,
            1.0,1.0,1.0,
    };


    GLubyte railgunColors[] = {
            255,255,255,255,
            255,255,255,255,
            255,255,255,255,
            255,255,255,255,
};

    GLfloat railgunTexture[] = {
    0, 0,
    1, 0,
    0, 1,
    1, 1,
};

    GLfloat *railgunNormal = makeNormals(railgunRay, ASIZE(railgunRay), 0, 0, 1);

    glBindTexture(GL_TEXTURE_2D, self.railgunTexture.name);

    glTranslatef(0.06, 2.9, -10);
    glRotatef(90, 1, 0, 0);
    glScalef(0.01, 25, 3);

    glVertexPointer(3, GL_FLOAT, 0, railgunRay);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, railgunColors);
    glTexCoordPointer(2, GL_FLOAT, 0, railgunTexture);
    glNormalPointer(GL_FLOAT, 0, railgunNormal);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisable(GL_ALPHA_TEST);
    glDisable(GL_BLEND);
    glDisable(GL_TEXTURE_2D);

    glPopMatrix();
}

-(void)initLighting
{
    GLfloat diffuse[]={0.9,0.9,0.9,1.0};
    GLfloat ambient[]={0.7,0.7,0.7,1.0};
    GLfloat emission[]={0.8,0.8,0.8,1.0};
    GLfloat pos[]={0.0,4.0,0.0,1.0};

    glLightfv(GL_LIGHT0,GL_POSITION,pos);
    glLightfv(GL_LIGHT0,GL_DIFFUSE,diffuse);
    glLightfv(GL_LIGHT0,GL_AMBIENT,ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, emission);

    glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.00025);

    glShadeModel(GL_SMOOTH);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    glLoadIdentity();
}


-(void)setClipping
{
    float aspectRatio;
    const float zNear = .1;
    const float zFar = 10000;
    const float fieldOfView = 50.0;
    GLfloat	size;

    CGRect frame = self.view.frame;

    aspectRatio=(float)frame.size.height/(float)frame.size.width;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    size = zNear * tanf(GLKMathDegreesToRadians (fieldOfView) / 2.0);

    glFrustumf(-size, size, -size /aspectRatio, size /aspectRatio, zNear, zFar);
    glViewport(0, 0, frame.size.height, frame.size.width);

    glMatrixMode(GL_MODELVIEW);
}

#pragma mark - Rotations

//- (NSUInteger)supportedInterfaceOrientations {
//    return UIInterfaceOrientationLandscapeLeft | UIInterfaceOrientationLandscapeRight;
//}
//
//- (BOOL)shouldAutorotate {
//    return YES;
//}
//
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

#pragma mark - Interactions

- (void)panGestureRecognizerActuated:(UIPanGestureRecognizer *)gest {
    CGPoint currentLocation = [gest locationInView:self.view];

    switch (gest.state) {
        case UIGestureRecognizerStatePossible:
            break;
        case UIGestureRecognizerStateBegan: {
            gl_oldRotAng = currentLocation;
        }
            break;
        case UIGestureRecognizerStateChanged: {
            [self setIsRailgunRayVisible:NO];

            CGFloat xValue = (gl_oldRotAng.x - currentLocation.x) / 8.0 + gl_endRotAng.x;
            CGFloat yValue = (gl_oldRotAng.y - currentLocation.y) / 8.0 + gl_endRotAng.y;
            gl_rotAng = CGPointMake(
                    fabsf(xValue) < 90 ? xValue : 90 * xValue / (fabsf(xValue)),
                    yValue
            );

        }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled: {
            gl_endRotAng = gl_rotAng;
        }
            break;
        case UIGestureRecognizerStateFailed:
            break;
    }
}

- (void)pinchGestureRecognizerActuated:(UIPinchGestureRecognizer *)gest {
    switch (gest.state) {
        case UIGestureRecognizerStatePossible:
            break;
        case UIGestureRecognizerStateBegan: {

        }
            break;
        case UIGestureRecognizerStateChanged: {
            gl_scale = gl_oldScale / (gest.scale);
        }
            break;
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled: {
            gl_oldScale = gl_scale;
        }
            break;
        case UIGestureRecognizerStateFailed:
            break;
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]]) {
        return YES;
    }
    return NO;
}


@end
