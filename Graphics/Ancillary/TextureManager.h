//
// Created by eugenedymov on 17.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>


@interface TextureManager : NSObject
+ (GLKTextureInfo *)loadTexture:(NSString *)name;

@end