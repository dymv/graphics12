//
// Created by eugenedymov on 15.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

GLfloat makeNormal(GLfloat value, int type);
GLvoid *makeNormals(GLfloat *array, unsigned long size, int byX, int byY, int byZ);
GLvoid *multiply2DArray(GLfloat *array, unsigned long size, GLfloat x, GLfloat y);

@interface Helpers : NSObject
@end