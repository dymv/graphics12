//
// Created by eugenedymov on 19.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <GLKit/GLKit.h>
#import "S23House.h"
#import "TextureManager.h"
#import "Helpers.h"

GLubyte s23colors[] = {
        122,0,0,255,
        122,122,122,255,
        122,122,122,255,
        122,122,122,255,
};

GLfloat sb1Texture[] = {
        0, 0,
        1, 0,
        0, 1,
        1, 1,
};

GLfloat sbuTexture[] = {
        .25, 0,
        .75, 0,
        0, 1,
        1, 1,
};

//main part

GLfloat sb1[] = {
        0,0,0,
        0.25,0,0,
        0,0.5,0,
        0.25, 0.5, 0
};

GLfloat sb2[] = {
        0, 0.5, 0,
        0.25, 0.5, 0,
        0, 1.25, -0.25,
        0.25, 1.25, -0.25,
};

GLfloat sb3[] = {
        0, 1.25, -0.25,
        0.25, 1.25, -0.25,
        0, 1.25, -0.5,
        0.25, 1.25, -0.5,
};

GLfloat sb4[] = {
        0.25,0,-0.5,
        0,0,-0.5,
        0.25, 1.25, -0.5,
        0,1.25,-0.5,
};

GLfloat sb5[] = {
        0,0,-0.5,
        0,0,0,
        0,0.5,-0.5,
        0,0.5,0,
};

GLfloat sb6[] = {
        0,0.5,-0.5,
        0,0.5,0,
        0,1.25,-0.5,
        0, 1.25, -0.25,
};


//helpers

GLfloat sbt[] = {
        -0.15, .62, -0.3,
        0.3,  .62, -0.3,
        -0.15, .8, -0.1,
        0.3,  .8, -0.1,
};

GLfloat sbu[] = {
        -0.15, .8, -0.1,
        0.3,  .8, -0.1,
        -0.45, 2., -0.75,
        0.7, 2., -0.75
};

@interface S23House ()
@property (nonatomic, retain) GLKTextureInfo *sbpTexture;
@property (nonatomic, retain) GLKTextureInfo *slateTexture;
@property (nonatomic, retain) GLKTextureInfo *triangleTexture;
@property (nonatomic, retain) House *mainHouse;


@end



@implementation S23House {
    GLfloat
            *sb1n,
            *sb2n,
            *sb3n,
            *sb4n,
            *sb5n,
            *sb6n,
            *sbtn,
            *sbun;
}
- (id)initWithPosition:(Vertex3D)position {
    self = [super init];
    if (self) {
        self.position = position;
        [self createObject];
    }

    return self;
}

+ (id)objectWithPosition:(Vertex3D)position {
    return [[[S23House alloc] initWithPosition:position] autorelease];
}


- (void)createObject {
    self.mainHouse = [House houseWithMainTexture:@"s23-texture" mainStretchFactor:1 sideTexture:@"s23-texture" sideStretchFactor:1 position:self.position rotationAngle:0 rotation:Vertex3DMake(0, 0, 0) generalScale:Vertex3DMake(4, 2, 4)];
    self.sbpTexture = [TextureManager loadTexture:@"gray-brick-texture"];
    self.slateTexture = [TextureManager loadTexture:@"slate-texture"];
    self.triangleTexture = [TextureManager loadTexture:@"pool-triangle-texture"];

    sb1n = makeNormals(sb1, ASIZE(sb1), 0, 0, 1);
    sb2n = makeNormals(sb2, ASIZE(sb2), 0, 1, 1);
    sb3n = makeNormals(sb3, ASIZE(sb3), 0, 1, 0);
    sb4n = makeNormals(sb4, ASIZE(sb4), 0, 0, -1);
    sb5n = makeNormals(sb5, ASIZE(sb5), -1, 0, 0);
    sb6n = makeNormals(sb6, ASIZE(sb6), -1, 0, 1);
    sbtn = makeNormals(sbt, ASIZE(sbt), 0, -1, 1);
    sbun = makeNormals(sbu, ASIZE(sbu), 0, 1, 1);
}

- (void)smallBottomPartExecute {
    glPushMatrix();
    glMatrixMode(GL_MODELVIEW);
    glScalef(0.5, 0.5, 0.5);

    glBindTexture(GL_TEXTURE_2D, self.sbpTexture.name);

    glVertexPointer(3, GL_FLOAT, 0, sb1);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s23colors);
    glTexCoordPointer(2, GL_FLOAT, 0, sb1Texture);
    glNormalPointer(GL_FLOAT, 0, sb1n);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, sb2);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s23colors);
    glTexCoordPointer(2, GL_FLOAT, 0, sb1Texture);
    glNormalPointer(GL_FLOAT, 0, sb2n);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, sb3);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s23colors);
    glTexCoordPointer(2, GL_FLOAT, 0, sb1Texture);
    glNormalPointer(GL_FLOAT, 0, sb3n);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, sb4);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s23colors);
    glTexCoordPointer(2, GL_FLOAT, 0, sb1Texture);
    glNormalPointer(GL_FLOAT, 0, sb4n);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, sb5);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s23colors);
    glTexCoordPointer(2, GL_FLOAT, 0, sb1Texture);
    glNormalPointer(GL_FLOAT, 0, sb5n);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, sb6);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s23colors);
    glTexCoordPointer(2, GL_FLOAT, 0, sb1Texture);
    glNormalPointer(GL_FLOAT, 0, sb6n);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glPushMatrix();

    glTranslatef(0.25, 0, 0);
    glScalef(-1, 1, 1);

    glVertexPointer(3, GL_FLOAT, 0, sb5);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s23colors);
    glTexCoordPointer(2, GL_FLOAT, 0, sb1Texture);
    glNormalPointer(GL_FLOAT, 0, sb5n);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, sb6);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s23colors);
    glTexCoordPointer(2, GL_FLOAT, 0, sb1Texture);
    glNormalPointer(GL_FLOAT, 0, sb6n);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glPopMatrix();

    glPopMatrix();
}

- (void)bottomPartTriangleExecute {
    glPushMatrix();
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GEQUAL, 0.5);

    glBindTexture(GL_TEXTURE_2D, self.triangleTexture.name);

    glVertexPointer(3, GL_FLOAT, 0, sbt);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s23colors);
    glTexCoordPointer(2, GL_FLOAT, 0, sb1Texture);
    glNormalPointer(GL_FLOAT, 0, sbtn);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisable(GL_ALPHA_TEST);
    glDisable(GL_BLEND);
    glPopMatrix();
}

- (void)bottomPartExecute {
    glPushMatrix();

    glTranslatef(0.25, 0, 0);

    glBindTexture(GL_TEXTURE_2D, self.slateTexture.name);

    glVertexPointer(3, GL_FLOAT, 0, sbu);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s23colors);
    glTexCoordPointer(2, GL_FLOAT, 0, sbuTexture);
    glNormalPointer(GL_FLOAT, 0, sbun);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glPushMatrix();
    glTranslatef(-0.25, 0, -0.25);
    glRotatef(-45, 0, 1, 0);
    glScalef(0.75, 1, 1.25);
    [self smallBottomPartExecute];
    glPopMatrix();

    glPushMatrix();
    glScalef(1, 1, 1.5);
    [self smallBottomPartExecute];
    glPopMatrix();

    glPushMatrix();
    glTranslatef(0.3, 0, -0.2);
    glRotatef(45, 0, 1, 0);
    glScalef(0.75, 1, 1.25);
    [self smallBottomPartExecute];
    glPopMatrix();

    glPushMatrix();
    [self bottomPartTriangleExecute];
    glPopMatrix();

    glPopMatrix();
}

- (void)bottomPartsSideExecute {
    glPushMatrix();

    glTranslatef(-0.05, 0, 0);

    glPushMatrix();
    glTranslatef(0.25, 0, 4.75);
    [self bottomPartExecute];
    glPopMatrix();

    glPushMatrix();
    glTranslatef(1.7, 0, 4.75);
    [self bottomPartExecute];
    glPopMatrix();

    glPushMatrix();
    glTranslatef(3.15, 0, 4.75);
    [self bottomPartExecute];
    glPopMatrix();

    glPopMatrix();
}

- (void)execute {
    glPushMatrix();

    glEnable(GL_TEXTURE_COORD_ARRAY);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_NORMAL_ARRAY);

    glPushMatrix();
    glTranslatef(self.position.x, self.position.y, self.position.z);

    [self bottomPartsSideExecute];

    glPushMatrix();
    glTranslatef(0, 0, 4);
    glRotatef(90, 0, 1, 0);
    [self bottomPartsSideExecute];
    glPopMatrix();

    glPushMatrix();
    glTranslatef(4, 0, 0);
    glRotatef(-90, 0, 1, 0);
    [self bottomPartsSideExecute];
    glPopMatrix();

    glPushMatrix();
    glTranslatef(4, 0, 4);
    glRotatef(180, 0, 1, 0);
    [self bottomPartsSideExecute];
    glPopMatrix();


    glPopMatrix();

    [self.mainHouse execute];

    glDisable(GL_NORMAL_ARRAY);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_TEXTURE_COORD_ARRAY);

    glPopMatrix();
}


@end