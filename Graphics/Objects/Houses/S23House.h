//
// Created by eugenedymov on 19.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "House.h"


@interface S23House : House

@property (nonatomic, assign) Vertex3D position;

- (id)initWithPosition:(Vertex3D)position;

+ (id)objectWithPosition:(Vertex3D)position;


@end