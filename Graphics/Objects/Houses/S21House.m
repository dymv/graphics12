//
// Created by eugenedymov on 17.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import <GLKit/GLKit.h>
#import "S21House.h"
#import "Defines.h"
#import "S21Defines.h"
#import "TextureManager.h"

extern GLvoid *makeNormals(GLfloat *array, unsigned long size, int byX, int byY, int byZ);

@interface S21House ()
@property (nonatomic, retain) GLKTextureInfo *s21Texture;

@end

@implementation S21House {
    GLfloat *s21k2v1normal;
    GLfloat *s21k2v2normal;
    GLfloat *s21k2v6normal;
    GLfloat *s21k2roofNormal;
    GLfloat *s21k2roof124Normal;
    GLfloat *s21k2roof35Normal;
}

- (void)createObject {
    self.s21Texture = [TextureManager loadTexture:kS21TextureName];
    
    s21k2v1normal = makeNormals(s21k2v1, ASIZE(s21k2v1), -1, 0, 1);
    s21k2v2normal = makeNormals(s21k2v2, ASIZE(s21k2v2), 1, 0, 1);
    s21k2v6normal = makeNormals(s21k2v6, ASIZE(s21k2v6), 0, 0, -1);
    s21k2roofNormal = makeNormals(s21k2roof, ASIZE(s21k2roof), 0, 1, 0);
    s21k2roof124Normal = makeNormals(s21k2roof124, ASIZE(s21k2roof124), 0, 1, 0);
    s21k2roof35Normal = makeNormals(s21k2roof35, ASIZE(s21k2roof35), 0, 1, 0);
}

- (void)partExecute {
    glMatrixMode(GL_MODELVIEW);

    glVertexPointer(3, GL_FLOAT, 0, s21k2v1);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s21colors);
    glTexCoordPointer(2, GL_FLOAT, 0, s21k2v1Texture);
    glNormalPointer(GL_FLOAT, 0, s21k2v1normal);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, s21k2v2);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s21colors);
    glTexCoordPointer(2, GL_FLOAT, 0, s21k2v2Texture);
    glNormalPointer(GL_FLOAT, 0, s21k2v2normal);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, s21k2v4);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s21colors);
    glTexCoordPointer(2, GL_FLOAT, 0, s21k2v4Texture);
    glNormalPointer(GL_FLOAT, 0, s21k2v4normal);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, s21k2v3);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s21colors);
    glTexCoordPointer(2, GL_FLOAT, 0, s21k2v3Texture);
    glNormalPointer(GL_FLOAT, 0, s21k2v3normal);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, s21k2v5);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s21colors);
    glTexCoordPointer(2, GL_FLOAT, 0, s21k2v5Texture);
    glNormalPointer(GL_FLOAT, 0, s21k2v5normal);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, s21k2roof124);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s21colors);
    glTexCoordPointer(2, GL_FLOAT, 0, s21k2roof124Texture);
    glNormalPointer(GL_FLOAT, 0, s21k2roof124Normal);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, s21k2roof35);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s21colors);
    glTexCoordPointer(2, GL_FLOAT, 0, s21k2roof35Texture);
    glNormalPointer(GL_FLOAT, 0, s21k2roof35Normal);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glTranslatef(0, 0, -1);

    glScalef(1, 1, -1);

    glVertexPointer(3, GL_FLOAT, 0, s21k2v1);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s21colors);
    glTexCoordPointer(2, GL_FLOAT, 0, s21k2v1Texture);
    glNormalPointer(GL_FLOAT, 0, s21k2v1normal);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, s21k2v2);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s21colors);
    glTexCoordPointer(2, GL_FLOAT, 0, s21k2v2Texture);
    glNormalPointer(GL_FLOAT, 0, s21k2v2normal);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, s21k2v4);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s21colors);
    glTexCoordPointer(2, GL_FLOAT, 0, s21k2v4Texture);
    glNormalPointer(GL_FLOAT, 0, s21k2v4normal);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glVertexPointer(3, GL_FLOAT, 0, s21k2roof124);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, s21colors);
    glTexCoordPointer(2, GL_FLOAT, 0, s21k2roof124Texture);
    glNormalPointer(GL_FLOAT, 0, s21k2roof124Normal);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

- (void)execute {
    glPushMatrix();

    glMatrixMode(GL_MODELVIEW);

    glEnable(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, self.s21Texture.name);

    glTranslatef(1, 0, 4);
    glScalef(2, 2, 1);

    [self partExecute];

    glPushMatrix();
    glTranslatef(2.5, 0, 0);
    glScalef(-1, 1, 1);
    [self partExecute];
    glPopMatrix();

    glPushMatrix();
    glTranslatef(2, 0, 9);
    glRotatef(90, 0, 1, 0);
    [self partExecute];
    glPopMatrix();

    glDisable(GL_TEXTURE_2D);

    glPopMatrix();
}


@end