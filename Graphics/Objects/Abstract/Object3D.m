//
// Created by eugenedymov on 17.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "Object3D.h"
#import "Macros.h"


@implementation Object3D {

}

- (void)createObject {
    INFO(@"To override");
}

- (void)execute {
    INFO(@"To override");
}


- (id)init {
    self = [super init];
    if (self) {
        [self createObject];
    }

    return self;
}


@end