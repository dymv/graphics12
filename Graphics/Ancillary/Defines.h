#import "OpenGLCommon.h"

#define kTexSize 2048.0

#define ASIZE(x) (sizeof((x)) / sizeof((x)[0]))

#define kS21TextureName @"s21-texture"
#define kRoadTextureName @"road-texture"
#define kGrassTextureName @"grass-texture"
#define kFullTextureName @"full-texture"