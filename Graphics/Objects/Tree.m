//
// Created by eugenedymov on 18.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <GLKit/GLKit.h>
#import "Tree.h"
#import "TextureManager.h"
#import "Helpers.h"
#import "Defines.h"
#import "Macros.h"

GLfloat treeVertices[] = {
        0.0,0.0,1.0,
        1.0,0.0,1.0,
        0.0,1.0,1.0,
        1.0,1.0,1.0,
};

GLubyte treeColors[] = {
        122,122,122,255,
        122,122,122,255,
        122,122,122,255,
        122,122,122,255,
};

GLfloat treeTextures[] = {
        0, 0,
        1, 0,
        0, 1,
        1, 1,
};

@interface Tree ()
@property (nonatomic, retain) NSString *textureName;
@property (nonatomic, retain) GLKTextureInfo *texture;

@property (nonatomic, assign) Vertex3D position;
@property (nonatomic, assign) CGFloat angle;
@property (nonatomic, assign) Vertex3D rotation;
@property (nonatomic, assign) Vertex3D scale;

@end

@implementation Tree {
    GLfloat *_treeNormals;
}

+ (Tree *)treeWithTexture:(NSString *)texture position:(Vertex3D)position angle:(CGFloat)angle rotation:(Vertex3D)rotation scale:(Vertex3D)scale {
    Tree *tree = [[[Tree alloc] init] autorelease];
    [tree setTextureName:texture];
    [tree setPosition:position];
    [tree setAngle:angle];
    [tree setRotation:rotation];
    [tree setScale:scale];

    return tree;
}

- (void)createObject {
    self.texture = [TextureManager loadTexture:@"tree01-texture"];
    _treeNormals = makeNormals(treeVertices, ASIZE(treeVertices), 0, 0, -1);
}

- (void)update {
    self.texture = [TextureManager loadTexture:self.textureName];
}

- (void)partExecute {
    glPushMatrix();

    glEnable(GL_TEXTURE_2D);

    glVertexPointer(3, GL_FLOAT, 0, treeVertices);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, treeColors);
    glTexCoordPointer(2, GL_FLOAT, 0, treeTextures);
    glNormalPointer(GL_FLOAT, 0, _treeNormals);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glPopMatrix();
}

- (void)completeExecute {
    glPushMatrix();

    glTranslatef(self.position.x, self.position.y, self.position.z);
    glScalef(self.scale.x, self.scale.y, self.scale.z);
    glRotatef(self.angle, self.rotation.x, self.rotation.y, self.rotation.z);


    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GEQUAL, 0.1);

    glBindTexture(GL_TEXTURE_2D, self.texture.name);
    [self partExecute];

    glTranslatef(-0.5, 0, 1.5);
    glRotatef(90, 0, 1, 0);

    [self partExecute];

    glDisable(GL_BLEND);
    glDisable(GL_ALPHA_TEST);
    glPopMatrix();
}

- (void)execute {
    [self completeExecute];
}


@end