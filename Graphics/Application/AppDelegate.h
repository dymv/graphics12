//
//  AppDelegate.h
//  Graphics
//
//  Created by Eugene Dymov on 28.10.12.
//  Copyright (c) 2012 Eugene Dymov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
