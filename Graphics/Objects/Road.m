//
// Created by eugenedymov on 17.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <GLKit/GLKit.h>
#import "Road.h"
#import "RoadDefines.h"
#import "Defines.h"
#import "Helpers.h"
#import "TextureManager.h"

@interface Road ()
@property (nonatomic, retain) GLKTextureInfo *t_asphalt;
@property (nonatomic, retain) GLKTextureInfo *t_road;
@property (nonatomic, retain) GLKTextureInfo *t_grass;
@end

@implementation Road {
    GLvoid *_roadNormal;
}

- (void)createObject {
    self.t_asphalt = [TextureManager loadTexture:kAsphaltTextureName];
    self.t_road = [TextureManager loadTexture:kRoadTextureName];
    self.t_grass = [TextureManager loadTexture:kGrassTextureName];
    _roadNormal = makeNormals(road, ASIZE(road), 0, -1, 0);
}

- (void)partExecute {
    glPushMatrix();

    glMatrixMode(GL_MODELVIEW);

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_NORMAL_ARRAY);

    glVertexPointer(3, GL_FLOAT, 0, road);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, roadColors);
    glTexCoordPointer(2, GL_FLOAT, 0, roadTexture);
    glNormalPointer(GL_FLOAT, 0, _roadNormal);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisable(GL_NORMAL_ARRAY);
    glDisable(GL_TEXTURE_2D);

    glPopMatrix();
}

- (void)asphaltExecute {
    glPushMatrix();

    glMatrixMode(GL_MODELVIEW);

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_NORMAL_ARRAY);

    glVertexPointer(3, GL_FLOAT, 0, asphalt);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, roadColors);
    glTexCoordPointer(2, GL_FLOAT, 0, asphaltTexture);
    glNormalPointer(GL_FLOAT, 0, _roadNormal);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    glDisable(GL_NORMAL_ARRAY);
    glDisable(GL_TEXTURE_2D);

    glPopMatrix();
}

- (void)execute {
    glPushMatrix();

    glBindTexture(GL_TEXTURE_2D, self.t_asphalt.name);
    [self asphaltExecute];

    glBindTexture(GL_TEXTURE_2D, self.t_road.name);
    [self partExecute];

    glPushMatrix();
    glTranslatef(-1, 0, 0);
    glBindTexture(GL_TEXTURE_2D, self.t_grass.name);
    [self partExecute];
    glPopMatrix();

    glPushMatrix();
    glBindTexture(GL_TEXTURE_2D, self.t_grass.name);
    glTranslatef(-3.5, 0, -2);
    glScalef(0.5, 1, 0.1);
    [self partExecute];
    glPopMatrix();

    glPushMatrix();
    glBindTexture(GL_TEXTURE_2D, self.t_grass.name);
    glTranslatef(3.5, 0, 9.5);
    glScalef(4.75, 1, 0.05);
    [self partExecute];
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-2, 0, 0);
    glBindTexture(GL_TEXTURE_2D, self.t_road.name);
    [self partExecute];
    glPopMatrix();


    glPopMatrix();
}

- (void)dealloc {
    free(_roadNormal);

    [super dealloc];
}


@end