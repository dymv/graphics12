//
// Created by eugenedymov on 15.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "Object3D.h"
#import "Defines.h"


@interface House : Object3D

@property (nonatomic, retain) NSString *mainTexture;
@property (nonatomic, retain) NSString *sideTexture;

@property (nonatomic, assign) NSInteger mainStretchFactor;
@property (nonatomic, assign) NSInteger sideStretchFactor;

@property (nonatomic, assign) Vertex3D position;
@property (nonatomic, assign) CGFloat angle;
@property (nonatomic, assign) Vertex3D rotation;
@property (nonatomic, assign) Vertex3D scale;


- (void)update;

+ (House *)houseWithMainTexture:(NSString *)mainTextureName
              mainStretchFactor:(NSInteger)mainStretchFactor
                    sideTexture:(NSString *)sideTextureName
              sideStretchFactor:(NSInteger)sideStretchFactor
                       position:(Vertex3D)position
                  rotationAngle:(CGFloat)angle
                       rotation:(Vertex3D)rotation
                   generalScale:(Vertex3D)scale;

@end