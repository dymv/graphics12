//
// Created by eugenedymov on 15.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <GLKit/GLKit.h>
#import "House.h"
#import "TextureManager.h"
#import "Helpers.h"
#import "Defines.h"

GLfloat mainVertices[] = {
        0.0,0.0,1.0,
        1.0,0.0,1.0,
        0.0,1.0,1.0,
        1.0,1.0,1.0,
};

GLubyte mainColors[] = {
        122,122,122,255,
        122,122,122,255,
        122,122,122,255,
        122,122,122,255,
};

GLfloat mainTextures[] = {
        0, 0,
        1, 0,
        0, 1,
        1, 1,
};

@interface House ()
@property (nonatomic, retain) GLKTextureInfo *mainTextureInfo;
@property (nonatomic, retain) GLKTextureInfo *sideTextureInfo;
@property (nonatomic, retain) GLKTextureInfo *roofTextureInfo;
@end

@implementation House {
    GLfloat
            *_mainNormals,
            *_sideNormals,
            *_roofNormals;

    GLfloat
            *_mainTextureArray,
            *_roofTextureArray;
}

+ (House *)houseWithMainTexture:(NSString *)mainTextureName mainStretchFactor:(NSInteger)mainStretchFactor sideTexture:(NSString *)sideTextureName sideStretchFactor:(NSInteger)sideStretchFactor position:(Vertex3D)position rotationAngle:(CGFloat)angle rotation:(Vertex3D)rotation generalScale:(Vertex3D)scale {
    House *house = [[[House alloc] init] autorelease];
    [house setMainTexture:mainTextureName];
    [house setMainStretchFactor:mainStretchFactor];
    [house setSideTexture:sideTextureName];
    [house setSideStretchFactor:sideStretchFactor];
    [house setPosition:position];
    [house setAngle:angle];
    [house setRotation:rotation];
    [house setScale:scale];

    [house update];

    return house;
}

- (void)dealloc {
    free(_mainNormals);
    free(_sideNormals);
    free(_roofNormals);

    free(_mainTextureArray);
    free(_roofTextureArray);

    [super dealloc];
}


- (void)createObject {

}

- (void)update {
    self.mainTextureInfo = [TextureManager loadTexture:self.mainTexture];
    self.sideTextureInfo = [TextureManager loadTexture:self.sideTexture];
    self.roofTextureInfo = [TextureManager loadTexture:@"roof-texture"];

    _mainNormals = makeNormals(mainVertices, ASIZE(mainVertices), 0, 0, 1);
    _sideNormals = makeNormals(mainVertices, ASIZE(mainVertices), 0, 0, 1);
    _roofNormals = makeNormals(mainVertices, ASIZE(mainVertices), 0, 0, 1);

    _mainTextureArray = multiply2DArray(mainTextures, ASIZE(mainTextures), self.mainStretchFactor, 1);
    _roofTextureArray = multiply2DArray(mainTextures, ASIZE(mainTextures), self.mainStretchFactor, 4);
}

- (void)execute {
    glPushMatrix();

    glMatrixMode(GL_MODELVIEW);

    glEnable(GL_TEXTURE_2D);

    glTranslatef(self.position.x, self.position.y, self.position.z);
    glRotatef(self.angle, self.rotation.x, self.rotation.y, self.rotation.z);
    glScalef(self.scale.x, self.scale.y, self.scale.z);

    [self v1Execute];
    [self v2Execute];
    [self v3Execute];
    [self v4Execute];
    [self rfExecute];

    glDisable(GL_TEXTURE_2D);

    glPopMatrix();
}



- (void)v1Execute {
    glPushMatrix();
    glScalef(self.mainStretchFactor * 1, 1, 1);

    glBindTexture(GL_TEXTURE_2D, self.mainTextureInfo.name);

    glVertexPointer(3, GL_FLOAT, 0, mainVertices);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, mainColors);
    glTexCoordPointer(2, GL_FLOAT, 0, _mainTextureArray);
    glNormalPointer(GL_FLOAT, 0, _mainNormals);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glPopMatrix();
}

- (void)v2Execute {
    glPushMatrix();
    glTranslatef(-1, 0, 1 );
    glRotatef(90, 0, 1, 0);
    glScalef(self.sideStretchFactor * 1, 1, 1);

    glBindTexture(GL_TEXTURE_2D, self.sideTextureInfo.name);
    glVertexPointer(3, GL_FLOAT, 0, mainVertices);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, mainColors);
    glTexCoordPointer(2, GL_FLOAT, 0, mainTextures);
    glNormalPointer(GL_FLOAT, 0, _mainNormals);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glPopMatrix();
}

- (void)v3Execute {
    glPushMatrix();
    glTranslatef(0, 0, -1 * self.sideStretchFactor * 1);
    [self v1Execute];
    glPopMatrix();
}

- (void)v4Execute {
    glPushMatrix();
    glTranslatef(self.mainStretchFactor, 0, 0);
    [self v2Execute];
    glPopMatrix();
}

- (void)rfExecute {
    glPushMatrix();
    glTranslatef(0, 2, 1 - self.sideStretchFactor);
    glScalef(self.mainStretchFactor, 1, self.sideStretchFactor);
    glRotatef(90, 1, 0, 0);

    glBindTexture(GL_TEXTURE_2D, self.roofTextureInfo.name);

    glVertexPointer(3, GL_FLOAT, 0, mainVertices);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, mainColors);
    glTexCoordPointer(2, GL_FLOAT, 0, _roofTextureArray);
    glNormalPointer(GL_FLOAT, 0, _mainNormals);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glPopMatrix();
}

@end