//
// Created by eugenedymov on 15.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

@interface Builder : NSObject

+ (Builder *)sharedBuilder;

- (void)execute;


@end