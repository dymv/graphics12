//
//  main.m
//  Graphics
//
//  Created by Eugene Dymov on 28.10.12.
//  Copyright (c) 2012 Eugene Dymov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
