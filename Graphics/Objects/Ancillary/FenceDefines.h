const GLfloat fence[] = {
        0.0,0.0,1.0,
        1.0,0.0,1.0,
        0.0,1.0,1.0,
        1.0,1.0,1.0,
};

const GLubyte fenceColors[] = {
        122, 122, 122, 255,
        122, 122, 122, 255,
        122, 122, 122, 255,
        122, 122, 122, 255,
};

const GLfloat fenceTextureM[] = {
        0,0,
        5,0,
        0,1,
        5,1
};