//
// Created by eugenedymov on 18.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "Object3D.h"
#import "Defines.h"


@interface Tree : Object3D

+ (Tree *)treeWithTexture:(NSString *)texture position:(Vertex3D)position angle:(CGFloat)angle rotation:(Vertex3D)rotation scale:(Vertex3D)scale;

@end