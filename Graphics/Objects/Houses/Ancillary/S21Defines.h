#import"Defines.h"

const GLubyte s21colors[] = {
        122, 122, 122, 255,
        122, 122, 122, 255,
        122, 122, 122, 255,
        122, 122, 122, 255,
};

const GLfloat s21k2v3normal[] = {
        0.7, -0.1, 2,
        1.0, -0.1, 1.5,
        0.7, 1.7, 2,
        1.0, 1.7, 1.5,
};

const GLfloat s21k2v1[] = {
        0.0, -0.1, 0,
        0.7, -0.1, 1,
        0.0, 1.7, 0,
        0.7, 1.7, 1,
};

const GLfloat s21k2v1Texture[] = {
        345.0 / kTexSize, (kTexSize - 453) / kTexSize,
        590.0 / kTexSize, (kTexSize - 453) / kTexSize,
        345.0 / kTexSize, (kTexSize - 9) / kTexSize,
        590.0 / kTexSize, (kTexSize - 9) / kTexSize,
};

const GLfloat s21k2v2[] = {
        0.7, -0.1, 1,
        1.0, -0.1, 0.5,
        0.7, 1.7, 1,
        1.0, 1.7, 0.5,
};

const GLfloat s21k2v2Texture[] = {
        590.0 / kTexSize, (kTexSize - 453) / kTexSize,
        739.0 / kTexSize, (kTexSize - 453) / kTexSize,
        590.0 / kTexSize, (kTexSize - 9) / kTexSize,
        739.0 / kTexSize, (kTexSize - 9) / kTexSize,
};

const GLfloat s21k2v3[] = {
        0.0, -0.1,-1,
        0.0, -0.1, 0,
        0.0, 1.7,-1,
        0.0, 1.7, 0,
};

const GLfloat s21k2v3Texture[] = {
        8.0 / kTexSize, (kTexSize - 453) / kTexSize,
        340.0 / kTexSize, (kTexSize - 453) / kTexSize,
        8.0 / kTexSize, (kTexSize - 9) / kTexSize,
        340.0 / kTexSize, (kTexSize - 9) / kTexSize,
};

const GLfloat s21k2v4[] = {
        1.0, -0.1, 0.5,
        0.65, -0.1, 0,
        1.0, 1.7, 0.5,
        0.65, 1.7, 0,
};

const GLfloat s21k2v4normal[] = {
        1.0 * 2, -0.1, 0.5* 2,
        0.5* 2, -0.1, 0* 2,
        1.0* 2, 1.7, 0.5* 2,
        0.5* 2, 1.7, 0* 2,
};

const GLfloat s21k2v4Texture[] = {
        345.0 / kTexSize, (kTexSize - 453) / kTexSize,
        590.0 / kTexSize, (kTexSize - 453) / kTexSize,
        345.0 / kTexSize, (kTexSize - 9) / kTexSize,
        590.0 / kTexSize, (kTexSize - 9) / kTexSize,
};

const GLfloat s21k2v5[] = {
        0.65, 1.7,-1,
        0.65, 1.7, 0,
        0.65, -0.1,-1,
        0.65, -0.1, 0,
};

const GLfloat s21k2v5normal[] = {
        0.6, -0.1,-1,
        0.6, -0.1, 0,
        0.6, 1.7,-1,
        0.6, 1.7, 0,
};

const GLfloat s21k2v5Texture[] = {
        8.0 / kTexSize, (kTexSize - 9) / kTexSize,
        340.0 / kTexSize, (kTexSize - 9) / kTexSize,
        8.0 / kTexSize, (kTexSize - 453) / kTexSize,
        340.0 / kTexSize, (kTexSize - 453) / kTexSize,
};

const GLfloat s21k2v6[] = {
        0.65, -0.1,-1,
        0.0, -0.1,-1,
        0.65, 1.7,-1,
        0.0, 1.7,-1,
};

const GLfloat s21k2v6Texture[] = {
        590.0 / kTexSize, (kTexSize - 453) / kTexSize,
        739.0 / kTexSize, (kTexSize - 453) / kTexSize,
        590.0 / kTexSize, (kTexSize - 9) / kTexSize,
        739.0 / kTexSize, (kTexSize - 9) / kTexSize,
};

const GLfloat s21k2roof124[] = {
        0.7, 1.7, 1,
        1.0, 1.7, 0.5,
        0.0, 1.7, 0,
        0.65, 1.7, 0,
};

const GLfloat s21k2roof124Texture[] = {
        590.0 / kTexSize, (kTexSize - 9) / kTexSize,
        739.0 / kTexSize, (kTexSize - 9) / kTexSize,
        590.0 / kTexSize, (kTexSize - 9) / kTexSize,
        739.0 / kTexSize, (kTexSize - 9) / kTexSize,
};

const GLfloat s21k2roof35[] = {
        0.0, 1.7, 0,
        0.65, 1.7, 0,
        0.0, 1.7,-1,
        0.65, 1.7, -1,
};

const GLfloat s21k2roof35Texture[] = {
        590.0 / kTexSize, (kTexSize - 9) / kTexSize,
        739.0 / kTexSize, (kTexSize - 9) / kTexSize,
        590.0 / kTexSize, (kTexSize - 9) / kTexSize,
        739.0 / kTexSize, (kTexSize - 9) / kTexSize,
};

const GLfloat s21k2roof[] = {
        0.7, 1.7, 1,
        1.0, 1.7, 0.5,
        0.0, 1.7, 0,
        1.0, 1.7, 0.5,
        0.0, 1.7,-1,
        0.65, 1.7,-1,
};

const GLfloat s21k2roofTexture[] = {
        590.0 / kTexSize, (kTexSize - 9) / kTexSize,
        739.0 / kTexSize, (kTexSize - 9) / kTexSize,
        590.0 / kTexSize, (kTexSize - 9) / kTexSize,
        739.0 / kTexSize, (kTexSize - 9) / kTexSize,
        590.0 / kTexSize, (kTexSize - 9) / kTexSize,
        739.0 / kTexSize, (kTexSize - 9) / kTexSize,
};