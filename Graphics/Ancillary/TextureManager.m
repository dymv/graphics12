//
// Created by eugenedymov on 17.11.12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <GLKit/GLKit.h>
#import "TextureManager.h"


@implementation TextureManager {

}

+ (GLKTextureInfo *)loadTexture:(NSString *)name {
    NSError *error;
    GLKTextureInfo *info;
    NSDictionary *options = @{GLKTextureLoaderOriginBottomLeft:@YES};

    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:@"png"];

    info = [GLKTextureLoader textureWithContentsOfFile:path options:options error:&error];

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);

    return info;
}

@end